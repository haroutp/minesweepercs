﻿using System;

namespace Minesweeper
{
    class Program
    {
        static void Main(string[] args)
        {
            bool[][] t1 = new bool[2][];
            t1[0] = new bool[]{false, false, false};
            t1[1] = new bool[]{false, false, false};
            

            int[][] n = minesweeper(t1);
        }

        static int[][] minesweeper(bool[][] matrix) {
            int rows = matrix.Length;
            int columns = matrix[0].Length;
            int[][] newMatrix = new int[rows][];
            for (int i = 0; i < matrix.Length; i++) {
                newMatrix[i] = new int[columns];
            }

            
            for(int i = 0; i < matrix.Length; i++){
                for(int j = 0; j < matrix[0].Length; j++){
                    //left
                    if(j > 0){
                        if(matrix[i][j - 1] == true){
                            newMatrix[i][j]++;
                        }
                    }
                    
                    //right
                    if(j < columns - 1){
                        if(matrix[i][j + 1] == true){
                            newMatrix[i][j]++;
                        }
                    }
                    
                    //above 
                    if(i > 0){
                        if(matrix[i - 1][j] == true){
                            newMatrix[i][j]++;
                        }
                    }
                    
                    //below
                    if(i < rows - 1){
                        if(matrix[i + 1][j] == true){
                            newMatrix[i][j]++;
                        }
                    }
                    
                    //below right 
                    if(i < rows - 1 && j < columns - 1){
                        if(matrix[i+1][j + 1] == true){
                            newMatrix[i][j]++;
                        }
                    }
                    
                    //down left 
                    if(i < rows - 1 && j > 0){
                        if(matrix[i+1][j - 1] == true){
                            newMatrix[i][j]++;
                        }
                    }
                    
                    //top right 
                    if(i > 0 && j < columns - 1){
                        if(matrix[i - 1][j + 1] == true){
                            newMatrix[i][j]++;
                        }
                    }
                    
                    //top left 
                    if(i > 0 && j > 0){
                        if(matrix[i - 1][j - 1] == true){
                            newMatrix[i][j]++;
                        }
                    }
                }
            }
            return newMatrix;
        }

        
    }
}
